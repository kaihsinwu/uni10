#ifndef __TEST_SYSTOOL_H__
#define __TEST_SYSTOOL_H__

#include <unistd.h>
#include <sys/syscall.h>

#include <string>
#include <fstream>

int getMaxRss();

#endif
