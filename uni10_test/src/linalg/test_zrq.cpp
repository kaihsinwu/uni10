#include "uni10.hpp"

#include <string>
#include <regex>

#include "test/test_linalg.h"
#include "test_tool/test_opts.h"

using namespace std;
using namespace uni10;

int main(int argc, char **argv)
{
    Opts options;

    if (options.parse(argc, argv)) {
        int Rnum = options.Rnum();
        int Cnum = options.Cnum();
        int ntest = options.ntest();

        cout << "             |                |            Error        \n"
             << "    M    N  n|   WTime  MaxRss| |I-Q*Q^T|   |M-R*Q|   R \n"
             << "========================================================\n";

        testRQ<uni10_complex128>(Rnum, Cnum, ntest, options.inplace());
    }

    return 0;
}
