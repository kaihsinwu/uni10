#include "uni10.hpp"

#include <string>
#include <regex>

#include "test/test_linalg.h"
#include "test_tool/test_opts.h"

using namespace std;
using namespace uni10;

int main(int argc, char **argv)
{
    Opts options;

    if (options.parse(argc, argv)) {
        int Rnum = options.Rnum();
        int Cnum = options.Cnum();
        int ntest = options.ntest();

        cout << "             |                |            Error            \n"
             << "    M    N  n|   WTime  MaxRss||I-U*U^T| |I-V*V^T||M-U*S*VT|\n"
             << "============================================================\n";

        testSVD<uni10_double64>(Rnum, Cnum, ntest, options.inplace());
    }

    return 0;
}
